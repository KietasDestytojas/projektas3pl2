﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Dal.DataAnnotations;

//TODO: Padaryti Regex telefono numeriui.
//TODO: Padaryti FillForm, kad veiktu su visais controlsais.
namespace Library.ViewModels
{
    public class BookReaderViewModel
    {
        [FormProperty(ControlName = "txtId", ControlType = InputType.TextBox)]
        public int? Id { get; set; }

        [FormProperty(ControlName = "txtFirstName", ControlType = InputType.TextBox)]
        public string FirstName { get; set; }

        [FormProperty(ControlName = "txtLastName", ControlType = InputType.TextBox)]
        public string LastName { get; set; }

        [FormProperty(ControlName = "txtPhone", ControlType = InputType.TextBox)]
        public string Phone { get; set; }

        [FormProperty(ControlName = "txtEmail", ControlType = InputType.TextBox)]
        public string Email { get; set; }
    }
}
