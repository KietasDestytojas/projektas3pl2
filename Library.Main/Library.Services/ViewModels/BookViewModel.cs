﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Dal.DataAnnotations;

namespace Library.ViewModels
{
    public class BookViewModel
    {
        [FormProperty(ControlName = "txtId", ControlType = InputType.TextBox)]
        public int? BookId { get; set; }

        public string Name { get; set; }

        public int ReleaseDate { get; set; }

        public int HoldDays { get; set; }

        public List<AuthorViewModel> BookAuthores { get; set; }
    }
}
