﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Models;
using Library.ViewModels;
using MySql.Data.MySqlClient;

namespace Library.Services
{
    public class AuthorService : DefaultService
    {
        public AuthorService()
        {
            
        }

        public AuthorViewModel SaveAuthor(AuthorViewModel author)
        {
            try
            {
                OpenConnection();
                MySqlCommand saveAuthor = new MySqlCommand();
                saveAuthor.Connection = libraryConnection;
                if (author.Id.HasValue)
                {
                    var cmdCommand = "update authores set Firstname = @firstname, Lastname = @lastname WHERE AuthorId = @authorId;";
                    saveAuthor.CommandText = cmdCommand;
                    saveAuthor.Parameters.AddWithValue("@firstname", author.FirstName);
                    saveAuthor.Parameters.AddWithValue("@lastname", author.LastName);
                    saveAuthor.Parameters.AddWithValue("@authorId", author.Id.Value);
                    saveAuthor.ExecuteNonQuery();
                } else
                {
                    var cmdCommand = "insert into authores (Firstname, Lastname) values (@firstname, @lastname);";
                    saveAuthor.CommandText = cmdCommand;
                    saveAuthor.Parameters.AddWithValue("@firstname", author.FirstName);
                    saveAuthor.Parameters.AddWithValue("@lastname", author.LastName);
                    saveAuthor.ExecuteNonQuery();
                    author.Id = Convert.ToInt32(saveAuthor.LastInsertedId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to save Author.");
            }
            CloseConnection();
            return author;
        }

        public List<AuthorViewModel> GetAuthores(int? authorId = null)
        {
            List<AuthorViewModel> authors = new List<AuthorViewModel>();
            var cmdText = "SELECT * FROM authores WHERE (@authorId IS NULL OR AuthorId = @authorId);";
            var cmdGetAuthores = new MySqlCommand(cmdText, libraryConnection);
            cmdGetAuthores.Parameters.AddWithValue("@authorId", authorId);

            OpenConnection();
            var authorReader = cmdGetAuthores.ExecuteReader();
            while(authorReader.Read())
            {
                authors.Add(new AuthorViewModel()
                {
                    Id = authorReader.GetInt32("AuthorId"),
                    FirstName = authorReader.GetString("FirstName"),
                    LastName = authorReader.GetString("LastName")
                });
            }
            CloseConnection();
            return authors;
        }

        public List<BookViewModel> GetBooks(int? authorId = null, int? bookId = null)
        {
            List<BookViewModel> books = new List<BookViewModel>();
            var booksCmdText = "";
            if (!authorId.HasValue)
            {
                booksCmdText = "SELECT * FROM books WHERE (@BookId IS NULL OR BookId = @BookId);";
            } else
            {
                booksCmdText = "select books.* from bookauthors inner join authores ON bookauthors.AuthorId = authores.AuthorId inner join books ON bookauthors.BookId = books.BookId where (@AuthorId IS NULL OR authores.AuthorId = @AuthorId) AND (@BookId IS NULL OR books.BookId = @BookId)";
            }
            
            var booksCmd = new MySqlCommand(booksCmdText, libraryConnection);
            booksCmd.Parameters.AddWithValue("@BookId", bookId);
            if (authorId.HasValue)
            {
                booksCmd.Parameters.AddWithValue("@AuthorId", authorId);
            }

            OpenConnection();
            var reader = booksCmd.ExecuteReader();
            while (reader.Read())
            {
                var book = new BookViewModel
                {
                    BookId = reader.GetInt32("BookId"),
                    Name = reader.GetString("Name"),
                    ReleaseDate = reader.GetInt32("ReleaseDate"),
                    HoldDays = reader.GetInt32("HoldDays"),
                    BookAuthores = new List<AuthorViewModel>()
                };

                books.Add(book);
            }
            CloseConnection();

            FillAuthores(books);
            
            return books;
        }

        private void FillAuthores(List<BookViewModel> books)
        {
            foreach (var book in books)
            {
                var authoresCmdText = "select authores.AuthorId, authores.FirstName, authores.LastName from bookauthors inner join authores ON bookauthors.AuthorId = authores.AuthorId inner join books ON bookauthors.BookId = books.BookId where books.BookId = @BookId;";

                var authoresCmd = new MySqlCommand(authoresCmdText, libraryConnection);
                authoresCmd.Parameters.AddWithValue("@BookId", book.BookId);

                OpenConnection();
                var authoresReader = authoresCmd.ExecuteReader();
                while (authoresReader.Read())
                {
                    book.BookAuthores.Add(new AuthorViewModel
                    {
                        Id = authoresReader.GetInt32("AuthorId"),
                        FirstName = authoresReader.GetString("FirstName"),
                        LastName = authoresReader.GetString("LastName")
                    });
                }

                CloseConnection();
            }
        }

        public BookViewModel SaveBook(BookViewModel book)
        {
            try
            {
                OpenConnection();
                if (book.BookId.HasValue)
                {
                    var bookUpdateCmdText = "UPDATE books SET Name = @Name, ReleaseDate = @ReleaseDate where BookId = @BookId;";

                    var bookUpdateCmd = new MySqlCommand(bookUpdateCmdText, libraryConnection);
                    bookUpdateCmd.Parameters.AddWithValue("@Name", book.Name);
                    bookUpdateCmd.Parameters.AddWithValue("@ReleaseDate", book.ReleaseDate);
                    bookUpdateCmd.Parameters.AddWithValue("@BookId", book.BookId);

                    bookUpdateCmd.ExecuteNonQuery();

                    var deleteBookAuthoresCmdText = "DELETE FROM bookauthors where BookId = @BookId";
                    var deleteBookAuthoresCmd = new MySqlCommand(deleteBookAuthoresCmdText, libraryConnection);
                    deleteBookAuthoresCmd.Parameters.AddWithValue("@BookId", book.BookId);
                    deleteBookAuthoresCmd.ExecuteNonQuery();

                    var bookAuthoresCmdText = "INSERT INTO bookauthors (AuthorId, BookId) VALUES ";
                    foreach (var bookAuthor in book.BookAuthores)
                    {
                        bookAuthoresCmdText += $"({bookAuthor.Id}, {book.BookId}),";
                    }
                    bookAuthoresCmdText = bookAuthoresCmdText.Substring(0, bookAuthoresCmdText.Length - 1) + ";";

                    var bookAuthoresCmd = new MySqlCommand(bookAuthoresCmdText, libraryConnection);
                    bookAuthoresCmd.ExecuteNonQuery();
                }
                else
                {
                    //insert
                    var bookSaveCmdText = "INSERT INTO books (Name, ReleaseDate, HoldDays) VALUES (@Name, @ReleaseDate, @HoldDays);";
                    var bookSaveCmd = new MySqlCommand(bookSaveCmdText, libraryConnection);
                    bookSaveCmd.Parameters.AddWithValue("@Name", book.Name);
                    bookSaveCmd.Parameters.AddWithValue("@ReleaseDate", book.ReleaseDate);
                    bookSaveCmd.Parameters.AddWithValue("@HoldDays", 14);
                    bookSaveCmd.ExecuteNonQuery();
                    var bookId = Convert.ToInt32(bookSaveCmd.LastInsertedId);
                    book.BookId = bookId;

                    var bookAuthoresCmdText = "INSERT INTO bookauthors (AuthorId, BookId) VALUES ";
                    foreach (var bookAuthor in book.BookAuthores)
                    {
                        bookAuthoresCmdText += $"({bookAuthor.Id}, {book.BookId}),";
                    }
                    bookAuthoresCmdText = bookAuthoresCmdText.Substring(0, bookAuthoresCmdText.Length - 1) + ";";

                    var bookAuthoresCmd = new MySqlCommand(bookAuthoresCmdText, libraryConnection);
                    bookAuthoresCmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Failed to save book. \n {ex.ToString()}");
            }
            CloseConnection();
            return book;
        }

        public void DeleteBook(int bookId)
        {
            var deleteBookCmdText = "DELETE FROM bookauthors where BookId = @BookId; DELETE FROM books where BookId = @BookId;";
            var deleteBookCmd = new MySqlCommand(deleteBookCmdText, libraryConnection);
            deleteBookCmd.Parameters.AddWithValue("@BookId", bookId);
            OpenConnection();
            deleteBookCmd.ExecuteNonQuery();
            CloseConnection();
        }

        public List<BookViewModel> GetBooks(BookSearchViewModel viewModel)
        {
            List<BookViewModel> books = new List<BookViewModel>();

            var booksCmdText = "SELECT b.* FROM Books b INNER JOIN BookAuthors ba ON b.BookId = ba.BookId INNER JOIN Authores a ON ba.AuthorId = a.AuthorId WHERE (@BookName IS NULL OR Name LIKE CONCAT('%', @BookName, '%')) AND (@BookReleaseDate IS NULL OR ReleaseDate = @BookReleaseDate) AND (@BookAuthor IS NULL OR(a.FirstName LIKE CONCAT('%', @BookAuthor, '%') OR a.LastName LIKE CONCAT('%', @BookAuthor, '%'))) GROUP BY b.BookId";

            var booksCmd = new MySqlCommand(booksCmdText, libraryConnection);
            booksCmd.Parameters.AddWithValue("@BookName", string.IsNullOrWhiteSpace(viewModel.BookName) ? null : viewModel.BookName);
            booksCmd.Parameters.AddWithValue("@BookReleaseDate", viewModel.ReleaseDate);
            booksCmd.Parameters.AddWithValue("@BookAuthor", string.IsNullOrWhiteSpace(viewModel.BookAuthor) ? null : viewModel.BookAuthor);

            OpenConnection();
            var reader = booksCmd.ExecuteReader();
            while (reader.Read())
            {
                var book = new BookViewModel
                {
                    BookId = reader.GetInt32("BookId"),
                    Name = reader.GetString("Name"),
                    ReleaseDate = reader.GetInt32("ReleaseDate"),
                    HoldDays = reader.GetInt32("HoldDays"),
                    BookAuthores = new List<AuthorViewModel>()
                };

                books.Add(book);
            }
            CloseConnection();

            FillAuthores(books);

            return books;
        }
    }
}
